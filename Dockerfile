FROM python:3-alpine

COPY . /code

RUN pip install -r /code/requirements.txt

ENTRYPOINT ["python3", "/code/build.py"]
