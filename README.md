### What
Build a dead simple material webpage from bids-validator json output


### How

```
pip3 install --user -r requirements.txt
python3 build.py data.json .
```

### Docker

```
docker run -it --rm -v $PWD:/data registry.git.mpib-berlin.mpg.de/krause/bids-validator-report /data/data.json /data/
```

### Screenshot

![screenshot](screenshot.png)

### Used in

https://git.mpib-berlin.mpg.de/citemplates/shared_artifacts/
https://citemplates.mpib.berlin/shared_artifacts/
