from staticjinja import Site
import json
import sys

def usage():
    print("usage: {} <json-data> <output-directory>".format(sys.argv[0]))
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage()
        sys.exit(1)
    with open(sys.argv[1]) as f:
        data = json.load(f)
    site = Site.make_site(env_globals=data, outpath=sys.argv[2])
    # enable automatic reloading
    site.render(use_reloader=False)
